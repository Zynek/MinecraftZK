﻿using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;

using Game.Terrain;

///<summary>
/// GameManager is at the top of everything. Manages activities of main game loop like starting the game, loading, suspending or quitting application.
/// Ideally it should be a singleton and can hold references to all other managers or singletons to make them more accesible.
///</summary>
public class GameManager : MonoBehaviour
{
    [SerializeField]
    private TerrainGenerator _terrainGen;
    public TerrainGenerator TerrainGenerator => _terrainGen;

    [SerializeField]
    private GameObject _playerPrefab;

    private GameObject _localPlayer;
    public GameObject LocalPlayer => _localPlayer;

    private const float RANDOM_START_POS_RANGE = 1000f;
    private Vector3 _startingPosition = Vector3.zero;

    [SerializeField, Range(10,1000)]
    private float _viewDistance = 10;

    private static GameManager _gameManager;
    public static GameManager Instance => _gameManager;
    private TerrainChunkMap _terrain = null;
    public TerrainChunkMap Terrain => _terrain;

    void Awake()
    {
        _gameManager = this;
        _terrain = new TerrainChunkMap(_startingPosition, GamePreferences.ViewDistance, _terrainGen);
    }
    
    void Start()
    {        
        StartNewGame();
    }

    void Update()
    {
        if (!SaveManager.LoadingSave)
        {
            _terrain.Update(_localPlayer.transform.position);
        }
    }

    public void StartNewGame()
    {
        _startingPosition = new Vector3(Mathf.Round (Random.Range(-RANDOM_START_POS_RANGE,RANDOM_START_POS_RANGE)),0f, Mathf.Round(Random.Range(-RANDOM_START_POS_RANGE,RANDOM_START_POS_RANGE)));
        _terrain.Update(_startingPosition);

        if(_localPlayer != null)
        {
            Destroy(_localPlayer);
        }
        SpawnLocalPlayer();
    }

    public void SpawnLocalPlayer()
    {
        float terrainHeightAtStartingPosition =  _terrainGen.SampleTerrainHeight(_startingPosition, out float baseTerrainHeight);
        Vector3 spawnPos = new Vector3(_startingPosition.x, terrainHeightAtStartingPosition, _startingPosition.z) + TerrainGenerator.BLOCK_EXTENTS;

        _localPlayer = Instantiate(_playerPrefab, spawnPos, Quaternion.identity);
    }

    void OnApplicationFocus(bool hasFocus)
    {
        if(!hasFocus)
        {
            GamePreferences.Save();
        }
    }

    void OnApplicationQuit()
    {
        GamePreferences.Save();
    }
}
