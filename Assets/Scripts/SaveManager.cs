﻿using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using Game.Terrain;

[Serializable]
public class SaveData
{    
    public SerializableVector3 PlayerPosition;
    public SerializableQuaternion PlayerRotation;

    //TODO add data from TerrainChunk to save changes in terrain
    public Dictionary<Hash128, TerrainChunk.TerrainChunkData> ModifiedTerrainChunks;
}

public static class SaveManager
{
    public const string DEFAULT_SAVE_NAME = "Game Save 1";
    public const string SAVE_SUFFIX = ".save";
    
    public static bool LoadingSave = false;

    public static void Save(string saveName = DEFAULT_SAVE_NAME)
    {
        string path = GetSavePath(saveName);
        var save = CollectSaveData();

        var bf = new BinaryFormatter();
        using (var stream = File.Create(path))
        {
            bf.Serialize(stream, save);
        }

        Debug.Log("Game saved to:" + path);
    }

    private static SaveData CollectSaveData()
    {
        var save = new SaveData();
        var player = GameManager.Instance.LocalPlayer;
        save.PlayerPosition = player.transform.position;
        save.PlayerRotation = player.transform.rotation;

        save.ModifiedTerrainChunks = GameManager.Instance.Terrain.CollectTerrainModifications();
        
        return save;
    }

    public static IEnumerator Load(string saveName = DEFAULT_SAVE_NAME)
    {
        string path = GetSavePath(saveName);
        
        if(File.Exists(path))
        {
            LoadingSave = true;
            SaveData save;
            var bf = new BinaryFormatter();
            using (var stream = File.OpenRead(path))
            {
                save = bf.Deserialize(stream) as SaveData;
            }

            GameManager.Instance.Terrain.UnloadTerrain();
            //Skip a frame
            yield return null;
            
            GameManager.Instance.Terrain.ModifiedTerrainChunks = save.ModifiedTerrainChunks;

            var player = GameManager.Instance.LocalPlayer;
            player.transform.position = save.PlayerPosition;
            player.transform.rotation = save.PlayerRotation;

            Debug.Log("Game loaded");
            LoadingSave = false;
        }
        else
        {
            Debug.Log("No game save found");
        }
    }

    public static string GetSavePath(string saveName)
    {
        return Application.persistentDataPath + "/" + saveName + SAVE_SUFFIX;
    }

}