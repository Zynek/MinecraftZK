﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    public static class PhysicsLayers
    {
        private static int TerrainLayerIndex = -1;
        private static int TerrainLayerMask = -1;

        public static int TerrainLayer()
        {
            if(TerrainLayerIndex == -1)
            {
                TerrainLayerIndex = LayerMask.NameToLayer("MTerrain");
                TerrainLayerMask = 1 << TerrainLayerIndex;
            }

            return TerrainLayerIndex;
        }

        public static int GetTerrainLayerMask()
        {
            if(TerrainLayerIndex == -1)
            {
                TerrainLayer();
            }
            return TerrainLayerMask;
        }
    }
}