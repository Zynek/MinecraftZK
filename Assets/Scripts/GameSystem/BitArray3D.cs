﻿using System;
using System.Collections;
using UnityEngine;

namespace GameSystem
{
    [Serializable]
    public class BitArray3D
    {
        private BitArray _array;
        //Shortcut for "dimension"
        private int _dimX;
        private int _dimY;
        private int _dimZ;

        public BitArray3D(int dimX, int dimY, int dimZ)
        {
            _dimX = dimX > 0 ? dimX : throw new ArgumentOutOfRangeException(nameof(dimX), dimX, string.Empty);
            _dimY = dimY > 0 ? dimY : throw new ArgumentOutOfRangeException(nameof(dimY), dimY, string.Empty);
            _dimZ = dimZ > 0 ? dimZ : throw new ArgumentOutOfRangeException(nameof(dimZ), dimZ, string.Empty);
            _array = new BitArray(dimX * dimY * dimZ);
        }

        public bool Get(int x, int y, int z) { CheckBounds(x, y, z); return _array[z * _dimX *_dimY + y * _dimX + x]; }
        public bool Set(int x, int y, int z, bool val) { CheckBounds(x, y, z); return _array[z * _dimX * _dimY + y * _dimX + x] = val; }
        public bool this[int x, int y, int z] { get { return Get(x, y, z); } set { Set(x, y, z, value); } }

        public bool this[Vector3Int pos] { get { return Get(pos.x, pos.y, pos.z); } set { Set(pos.x, pos.y, pos.z, value); } }

        public void ForEach(Action<int, int, int, bool> doTheThing)
        {
            for(int x = 0; x < _dimX; x++)
            {
                for(int y = 0; y < _dimY; y++)
                {
                    for(int z = 0; z < _dimZ; z++)                
                    {
                        doTheThing(x,y,z, Get(x,y,z));
                    }
                }
            }
        }
     

        public void CheckBounds(int x, int y, int z)
        {        
            bool isOut = false;
        
            if (x < 0 || x >= _dimX)
            {
                isOut = true;
            }
            if (y < 0 || y >= _dimY)
            {
                isOut = true;
            }
            if (z < 0 || z >= _dimZ)
            {
                isOut = true;
            }

            if(isOut)
                throw new IndexOutOfRangeException("Out of bounds at x:" + x + " y:" + y + " z:" + z +
                                                   "\nDims are x:" + _dimX + " y:" + _dimY +" z:"+ _dimZ );
        }

        public bool IsInBounds(Vector3Int pos)
        {   
            if (pos.x < 0 || pos.x >= _dimX)
            {
                return false;
            }
            if (pos.y < 0 || pos.y >= _dimY)
            {
                return false;
            }
            if (pos.z < 0 || pos.z >= _dimZ)
            {
                return false;
            }

            return true;                                       
        }

    }
}

