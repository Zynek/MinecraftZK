﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePreferences 
{
    public const string VIEW_DIST_KEY = "ViewDistance";

    public static float ViewDistance 
    {
        get
        {
            float viewDist = PlayerPrefs.GetFloat(VIEW_DIST_KEY, 100f);
            Debug.Log("Loading view distance:" + viewDist);
            return viewDist;
        }
        set
        {
            PlayerPrefs.SetFloat(VIEW_DIST_KEY, value);
        }
    }

    public static void Save()    
    {
        PlayerPrefs.Save();
    }

}
