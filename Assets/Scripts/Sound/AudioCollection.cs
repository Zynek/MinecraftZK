﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class AudioCollection : ScriptableObject
{
    public List<AudioClip> _audioList = new List<AudioClip>();

    public AudioClip GetRandomClip()
    {
        var count = _audioList.Count;
        if (count < 1)
            return null;
        
        //FUKING UNITY SHIT
        //THIS FUKIN METHOD IS RANDOM.RANGE(INCLUSIVE,EXCLUSIVE)
        //TALK ABOUT CONSISTENCY YOU PIECES OF SHIT!!!!!!!!!!!!!!!!!!!!!!!!!!
        return _audioList[Random.Range(0, count)];
    }

}
