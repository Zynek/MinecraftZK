﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SoundPlayer))]
public class AltitudeBasedAmbientPlayer : MonoBehaviour
{
    public List<AudioCollection> Collections = new List<AudioCollection>();
    public AnimationCurve Blend = AnimationCurve.Linear(0f,0f,1f,1f);
    
    public float MinHeight = 0f;
    public float MaxHeight = 30f;
    public float BlendThreshold = 0.2f;

    private SoundPlayer _player = default;
    private int _collectionIndex = -1;
    private float _blendValDuringLastChange = -1f;

    private void Start()
    {
        _player = GetComponent<SoundPlayer>();
    }

    private void Update()
    {
        float newBlendValue = Blend.Evaluate((transform.position.y - MinHeight) / (MaxHeight - MinHeight));
        
        int newIndex = Mathf.FloorToInt(newBlendValue * Collections.Count);
        //BlendThreshold prevents from switching between ambients too often when player is standing at the tipping point
        if (_collectionIndex != newIndex && Mathf.Abs(Mathf.Abs(newBlendValue) - Mathf.Abs(_blendValDuringLastChange)) > BlendThreshold)
        {
            _collectionIndex = newIndex;
            _player.SwitchCollectionTo(Collections[newIndex]);
            _blendValDuringLastChange = newBlendValue;
        }
    }
    
}
