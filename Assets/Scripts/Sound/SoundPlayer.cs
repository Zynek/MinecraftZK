﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(AudioSource))]
public class SoundPlayer : MonoBehaviour
{
    struct AudioSourceData
    {
        public AudioSource Source;
        public Coroutine FadeCoro;
    }
    
    [SerializeField]private AudioSource _source1 = default;
    private AudioSourceData _sourceData1;
    
    [SerializeField]private AudioSource _source2 = default;
    private AudioSourceData _sourceData2;
    
    [SerializeField] private AudioCollection _currentCollection = default;

    [SerializeField] private float _fadeDuration = 3f;
    private float _fadeStartTime = 0f;
    
    [FormerlySerializedAs("_musicTime")] [SerializeField] private float _playTime = 30f;
    private float _startTime = 0f;
    
    [SerializeField] private float _pauseTime = 0f;
    private float _startPauseTime = 0f;

    private bool _isPlaying = false;
    
    private float _defaultVolume = 1f;


    public static SoundPlayer Instance;
    
    void Start()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
        
        DontDestroyOnLoad(gameObject);
        
        _sourceData1.Source = _source1;
        _sourceData2.Source = _source2;
        
        _defaultVolume = _source1.volume;
        
        SetupSource(_source1);
        SetupSource(_source2);
        
        _startTime = Time.time;
        _startPauseTime = _startTime - _pauseTime;
        
        Play();

    }

    public void SwitchCollectionTo(AudioCollection newCollection)
    {
        _currentCollection = newCollection;
        Play();
    }
    
    private void SetupSource(AudioSource audioSource)
    {
        audioSource.loop = true;
        audioSource.volume = 0f;
    }

    protected virtual void Play()
    {
        var occupiedSourceData = GetOccupiedSourceData();
        var freeSourceData = GetFreeSourceData();
        var freeSource = freeSourceData.Source;
        
        FadeOut(occupiedSourceData);
        
        freeSource.clip = _currentCollection.GetRandomClip();
        freeSource.Play();
        
        FadeIn(freeSourceData);
        
        _startTime = Time.time;
        _isPlaying = true;
    }

    private void Update()
    {
        if (_isPlaying)
        {
            if (Time.time > _startTime + _playTime)
            {
                Pause();
            }
        }
        else
        {
            if (Time.time > _startPauseTime + _pauseTime)
            {
                Play();
            }
        }
    }

    protected virtual void Pause()
    {
        FadeOut(_sourceData1);
        FadeOut(_sourceData2);

        _startPauseTime = Time.time;
        _isPlaying = false;
    }
    
    private void FadeIn(AudioSourceData sourceData)
    {
        //Debug.Log("FadeIn:" + sourceData.Source.clip.name);
        StartFade(_defaultVolume, sourceData);
    }
    
    private void FadeOut(AudioSourceData sourceData)
    {
        if (!sourceData.Source.isPlaying)
        {
            sourceData.Source.volume = 0f;
            return;
        }
        
        //Debug.Log("FadeOut:" + sourceData.Source.clip.name);
        StartFade(0f, sourceData);
    }
    
    private void StartFade(float targetValue, AudioSourceData targetData)
    {
        if (targetData.FadeCoro != default)
        {
            StopCoroutine(targetData.FadeCoro);
            targetData.FadeCoro = default;
        }
        
        targetData.FadeCoro = StartCoroutine(Fade(targetValue, targetData.Source));
    }

    IEnumerator Fade(float targetValue, AudioSource targetSource)
    {
        var fadeVol = targetSource.volume;
        _fadeStartTime = Time.time;
        
        while (Time.time < _fadeStartTime + _fadeDuration)
        {
            targetSource.volume = Mathf.Lerp(fadeVol, targetValue, (Time.time - _fadeStartTime) / _fadeDuration);
            yield return new WaitForEndOfFrame();
        }
        targetSource.volume = targetValue;
    }

    private AudioSourceData GetOccupiedSourceData()
    {
        if (_source1.volume > 0f)
            return _sourceData1;

        return _sourceData2;
    }

    private AudioSourceData GetFreeSourceData()
    {
        if (_source1.volume <= 0f)
            return _sourceData1;

        return _sourceData2;
    }
    
}
