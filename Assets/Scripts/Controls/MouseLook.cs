﻿using System;
using System.Threading;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.Serialization;
using UnityStandardAssets.CrossPlatformInput;

namespace Game.Controls
{
    [Serializable]
    public class MouseLook
    {
        public float Sensitivity = 1f;
        public float GamepadSensitivity = 5f;
        public bool ClampVerticalRotation = true;
        public float MinimumX = -90F;
        public float MaximumX = 90F;

        private Quaternion m_CharacterTargetRot;
        private Quaternion m_CameraTargetRot;
        private Vector2 _mouseDelta = Vector2.zero;
        private bool _controlledByGamepad;

        public void Init(Transform character, Transform camera)
        {
            m_CharacterTargetRot = character.localRotation;
            m_CameraTargetRot = camera.localRotation;
            
            GameInputSingleton.Input.PC.LookAround.performed += UpdateRotationInput;
            GameInputSingleton.Input.PC.LookAround.canceled += UpdateRotationInput;
        }
        
        public void LookRotation(Transform character, Transform camera)
        {         
            if(Cursor.visible)
                return;

            float sensitivity = _controlledByGamepad ? GamepadSensitivity : Sensitivity;
            
            m_CharacterTargetRot *= Quaternion.Euler (0f, _mouseDelta.x * sensitivity, 0f);
            m_CameraTargetRot *= Quaternion.Euler (-_mouseDelta.y * sensitivity, 0f, 0f);

            if(ClampVerticalRotation)
                m_CameraTargetRot = ClampRotationAroundXAxis (m_CameraTargetRot);

            character.localRotation = m_CharacterTargetRot;
            camera.localRotation = m_CameraTargetRot;
        }

        private Quaternion ClampRotationAroundXAxis(Quaternion q)
        {
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w = 1.0f;

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan (q.x);

            angleX = Mathf.Clamp (angleX, MinimumX, MaximumX);

            q.x = Mathf.Tan (0.5f * Mathf.Deg2Rad * angleX);

            return q;
        }

        private void UpdateRotationInput(InputAction.CallbackContext obj)
        {
            _mouseDelta = obj.ReadValue<Vector2>();
            _controlledByGamepad = obj.control is StickControl;
        }

        ~MouseLook()
        {
            GameInputSingleton.Input.PC.LookAround.performed -= UpdateRotationInput;
            GameInputSingleton.Input.PC.LookAround.canceled -= UpdateRotationInput;
        }
        

    }
}
