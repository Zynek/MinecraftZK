﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

using Game.Characters;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.Serialization;

namespace Game.Controls
{
    [RequireComponent(typeof (Character))]
    public class PlayerController : CharController
    {
        public Camera _cam;
        public Game.Controls.MouseLook _mouseLook = new Game.Controls.MouseLook();
        private Vector2 _movementDirection;
        private bool _controlledByGamepad = false;

        private void Start()
        {
            _mouseLook.Init (transform, _cam.transform);
            GameInputSingleton.Input.PC.Walk.performed += UpdateInput;
            GameInputSingleton.Input.PC.Walk.canceled += UpdateInput;
            
            GameInputSingleton.Input.PC.Jump.performed += OnJumpAction;
        }

        private void Update()
        {
            _mouseLook.LookRotation(_character.transform, _cam.transform);
            
            _character.SetPickingRay(new Ray(_cam.transform.position, _cam.transform.forward));
            
            _character._movementSettings.UpdateDesiredTargetSpeed(_movementDirection);

            float oldYRotation = transform.eulerAngles.y;

            Vector3 desiredMove = _cam.transform.forward*_movementDirection.y + _cam.transform.right*_movementDirection.x;
            _character.UpdateMovementDirection(desiredMove, oldYRotation, _controlledByGamepad);
        }

        private void OnJumpAction(InputAction.CallbackContext obj)
        {
            _character.Jump();
        }

        private void UpdateInput(InputAction.CallbackContext obj)
        {
            _movementDirection = obj.ReadValue<Vector2>();
            _controlledByGamepad = obj.control is StickControl;
        }

    }
}