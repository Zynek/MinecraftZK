﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game.Characters;

namespace Game.Controls
{
    ///<summary>
    /// Base class for controlling a Character
    ///</summary>
    public abstract class CharController : MonoBehaviour
    {
        [SerializeField]
        protected Character _character;

    }
}