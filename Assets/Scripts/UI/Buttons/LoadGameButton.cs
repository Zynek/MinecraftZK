﻿using UnityEngine;
using GameSystem;

namespace Game.UI
{
    public class LoadGameButton : MonoBehaviour
    {        
        public void OnClickLoadGame()
        {
            if(!SaveManager.LoadingSave)
                StartCoroutine(SaveManager.Load());
        }
    }
}