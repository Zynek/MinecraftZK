﻿using UnityEngine;

public class SaveGameButton : MonoBehaviour
{
    public void OnClickSaveGame()
    {
        SaveManager.Save();
    }
}
