﻿using UnityEngine;

public class NewGameButton : MonoBehaviour
{
    public void OnClickNewGame()
    {
        GameManager.Instance.StartNewGame();
    }
}
