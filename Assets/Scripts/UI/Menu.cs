﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace Game.UI
{
    public class Menu : MonoBehaviour
    {
        [SerializeField] private GameObject _menuFirstSelected = default;
        [SerializeField] private GameObject _panel = null;
        private bool _cursorLocked = true;

        private void Start()
        {
            GameInputSingleton.Input.PC.ToggleMenu.performed += OnShowMenuAction;
        }

        private void OnDestroy()
        {
            GameInputSingleton.Input.PC.ToggleMenu.performed -= OnShowMenuAction;
        }
        /*
        void Update()
        {
            //CursorLockUpdate();
        }
        */
        public void CloseMenu()
        {
            _cursorLocked = true;
            CursorLockUpdate();
        }

        private void OnShowMenuAction(InputAction.CallbackContext obj)
        {
            _cursorLocked = !_cursorLocked;
            CursorLockUpdate();
        }
        
        private void CursorLockUpdate()
        {
            if (_cursorLocked)
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
            else if (!_cursorLocked)
            {
                Cursor.lockState = CursorLockMode.None;
            }
            
            Cursor.visible = !_cursorLocked;
            _panel.SetActive(Cursor.visible);
            
            EventSystem.current.SetSelectedGameObject(Cursor.visible ? _menuFirstSelected : null);
        }
    }
}