﻿using UnityEngine;

using Game.Characters;

namespace Game.UI
{
    public class MiningModeUI : MonoBehaviour
    {
        [SerializeField] private GameObject _container;
        private MiningMode _mode = null;
        void Awake()
        {
            CharacterMode.CharModeChanged+= OnPlayerModeChanged;
        }

        private void OnPlayerModeChanged(CharacterMode mode)
        {
            MiningMode miningMode = mode as MiningMode;
            if(miningMode != null)
            {
                _mode = miningMode;
            }
            _container.SetActive(miningMode != null);
            
        }
                
        public void OnDestroy()
        {
            CharacterMode.CharModeChanged-= OnPlayerModeChanged;
        }
    }
}