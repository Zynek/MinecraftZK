﻿using UnityEngine;
using UnityEngine.UI;

public class ViewDistanceSlider : MonoBehaviour
{       
    private Slider _slider;
    [SerializeField]
    private Text _valueText;
    void Start()
    {
        _slider = GetComponentInChildren<Slider>();
        _slider.value = GameManager.Instance.Terrain.ViewDistance;
        _valueText.text = ((int)_slider.value).ToString();
    }

    public void OnValueChanged(float newVal)
    {
        if(newVal != GameManager.Instance.Terrain.ViewDistance)
        {
            GameManager.Instance.Terrain.SetViewDistance(newVal);
            newVal = GameManager.Instance.Terrain.ViewDistance;
            GamePreferences.ViewDistance = newVal;
            _valueText.text = ((int)newVal).ToString();
        }
    }
}
