﻿using UnityEngine;
using UnityEngine.UI;

using Game.Characters;

namespace Game.UI
{
    public class BuildModeUI : MonoBehaviour
    {
        [SerializeField] private Text _blockSelectionText;
        [SerializeField] private GameObject _builderUI;
        private BlockPrototype _lastProto = null;
        private BuilderMode _mode = null;
        void Awake()
        {
            CharacterMode.CharModeChanged+= OnPlayerModeChanged;
        }

        private void OnPlayerModeChanged(CharacterMode mode)
        {
            BuilderMode builderMode = mode as BuilderMode;
            if(builderMode != null)
            {
                _mode = builderMode;
            }
            _builderUI.SetActive(builderMode != null);
            
        }
        
        void Update()
        {
            if(_mode != null)
            {
                if(_lastProto != _mode.ChosenBlock)
                {                    
                    _lastProto = _mode.ChosenBlock;
                    _blockSelectionText.text = _lastProto.name;
                }
            }
        }
        
        public void OnDestroy()
        {
            CharacterMode.CharModeChanged-= OnPlayerModeChanged;
        }
    }
}