﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    public class BlockCounter : MonoBehaviour
    {
        [SerializeField]
        private Text _countText;
        
        // Update is called once per frame
        void Update()
        {
            _countText.text = GameManager.Instance.TerrainGenerator.BlockCount.ToString();
        }
    }
}