﻿using System;
using UnityEngine;

namespace Serialization
{
  [Serializable]
  public struct SerializableBounds
  {
    private SerializableVector3 m_Center;
    private SerializableVector3 m_Extents;

    /// <summary>
    ///   <para>Creates a new Bounds.</para>
    /// </summary>
    /// <param name="center">The location of the origin of the Bounds.</param>
    /// <param name="size">The dimensions of the Bounds.</param>
    public SerializableBounds(Vector3 center, Vector3 size)
    {
      this.m_Center = center;
      this.m_Extents = size * 0.5f;
    }

    public override int GetHashCode()
    {
      Vector3 vector3 = this.center;
      int hashCode = vector3.GetHashCode();
      vector3 = this.extents;
      int num = vector3.GetHashCode() << 2;
      return hashCode ^ num;
    }

    public override bool Equals(object other) => other is SerializableBounds other1 && this.Equals(other1);

    public bool Equals(SerializableBounds other) => this.center.Equals(other.center) && this.extents.Equals(other.extents);

    /// <summary>
    ///   <para>The center of the bounding box.</para>
    /// </summary>
    public Vector3 center
    {
      get => this.m_Center;
      set => this.m_Center = value;
    }

    /// <summary>
    ///   <para>The total size of the box. This is always twice as large as the extents.</para>
    /// </summary>
    public Vector3 size
    {
      get => this.m_Extents * 2f;
      set => this.m_Extents = value * 0.5f;
    }

    /// <summary>
    ///   <para>The extents of the Bounding Box. This is always half of the size of the Bounds.</para>
    /// </summary>
    public Vector3 extents
    {
      get => this.m_Extents;
      set => this.m_Extents = value;
    }

    /// <summary>
    ///   <para>The minimal point of the box. This is always equal to center-extents.</para>
    /// </summary>
    public SerializableVector3 min
    {
      get => this.center - this.extents;
      set => this.SetMinMax(value, this.max);
    }

    /// <summary>
    ///   <para>The maximal point of the box. This is always equal to center+extents.</para>
    /// </summary>
    public SerializableVector3 max
    {
      get => this.center + this.extents;
      set => this.SetMinMax(this.min, value);
    }

    public static bool operator ==(SerializableBounds lhs, SerializableBounds rhs) => lhs.center == rhs.center && lhs.extents == rhs.extents;

    public static bool operator !=(SerializableBounds lhs, SerializableBounds rhs) => !(lhs == rhs);

    /// <summary>
    ///   <para>Sets the bounds to the min and max value of the box.</para>
    /// </summary>
    /// <param name="min"></param>
    /// <param name="max"></param>
    public void SetMinMax(Vector3 min, Vector3 max)
    {
      this.extents = (max - min) * 0.5f;
      this.center = min + this.extents;
    }

    /// <summary>
    ///   <para>Grows the Bounds to include the point.</para>
    /// </summary>
    /// <param name="point"></param>
    public void Encapsulate(SerializableVector3 point) => this.SetMinMax(Vector3.Min(this.min, point), Vector3.Max(this.max, point));

    /// <summary>
    ///   <para>Grow the bounds to encapsulate the bounds.</para>
    /// </summary>
    /// <param name="bounds"></param>
    public void Encapsulate(Bounds bounds)
    {
      this.Encapsulate(bounds.center - bounds.extents);
      this.Encapsulate(bounds.center + bounds.extents);
    }

    /// <summary>
    ///   <para>Expand the bounds by increasing its size by amount along each side.</para>
    /// </summary>
    /// <param name="amount"></param>
    public void Expand(float amount)
    {
      amount *= 0.5f;
      this.extents += new Vector3(amount, amount, amount);
    }

    /// <summary>
    ///   <para>Expand the bounds by increasing its size by amount along each side.</para>
    /// </summary>
    /// <param name="amount"></param>
    public void Expand(Vector3 amount) => this.extents += amount * 0.5f;

    /// <summary>
    ///   <para>Does another bounding box intersect with this bounding box?</para>
    /// </summary>
    /// <param name="bounds"></param>
    public bool Intersects(Bounds bounds) => (double) this.min.x <= (double) bounds.max.x && (double) this.max.x >= (double) bounds.min.x && ((double) this.min.y <= (double) bounds.max.y && (double) this.max.y >= (double) bounds.min.y) && (double) this.min.z <= (double) bounds.max.z && (double) this.max.z >= (double) bounds.min.z;

    public bool Contains(Vector3 point)
    {
      Vector3 minV = min;
      Vector3 maxV = max;

      return point.x >= minV.x && point.x <= maxV.x
       && point.y >= minV.y && point.y <= maxV.y
       && point.z >= minV.z && point.z <= maxV.z;
    }
    /*
    public bool Contains(Vector3 point)
    {
      Vector3 offset = point - center;

      return Mathf.Abs(offset.x) <= m_Extents.x
        && Mathf.Abs(offset.y) <= m_Extents.y
        && Mathf.Abs(offset.z) <= m_Extents.z;
    }
    */
    
  }
}
