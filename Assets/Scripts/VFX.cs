﻿using UnityEngine;

public class VFX : MonoBehaviour
{
    [SerializeField]
    ParticleSystem _leadingParticleSys = null;
    void Start()
    {
        if(_leadingParticleSys == null)
            _leadingParticleSys = GetComponent<ParticleSystem>();
        
        if(_leadingParticleSys != null)
        {
            _leadingParticleSys.Play();
        }
        else
        {
            Debug.LogError("VFX:" + name + " is missing particle system !");
            Destroy(gameObject);
        }
    }

    void Update()
    {
        if(_leadingParticleSys != null)
        {
            if(_leadingParticleSys.isStopped)
            {
                Destroy(gameObject);
            }
        }
    }
}
