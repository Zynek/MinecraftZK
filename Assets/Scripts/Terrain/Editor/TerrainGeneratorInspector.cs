﻿using System.Collections;
using System.Collections.Generic;
using Serialization;
using UnityEngine;
using UnityEditor;

namespace Game.Terrain
{
    [CustomEditor(typeof(TerrainGenerator))]
    public class TerrainGeneratorInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if(GUILayout.Button("Bigger test terrain generator"))
            {
                TerrainGenerator tg = (TerrainGenerator)target;
                tg.ClearTerrain();
                tg.GenerateArea(new SerializableBounds(Vector3.zero, new Vector3(500,50,500)));
            }

            if(GUILayout.Button("Test terrain generator"))
            {
                TerrainGenerator tg = (TerrainGenerator)target;
                tg.ClearTerrain();
                tg.GenerateArea(new SerializableBounds(Vector3.zero, new Vector3(50,50,50)));
            }

            if(GUILayout.Button("Small test terrain generator"))
            {
                TerrainGenerator tg = (TerrainGenerator)target;
                tg.ClearTerrain();
                tg.GenerateArea(new SerializableBounds(Vector3.zero, new Vector3(5,5,5)));
            }

            if(GUILayout.Button("Clear terrain"))
            {
                TerrainGenerator tg = (TerrainGenerator)target;
                tg.ClearTerrain();
            }
        }

    }
}