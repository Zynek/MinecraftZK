﻿using System.Collections.Generic;
using UnityEngine;

using GameSystem;
using Serialization;

namespace Game.Terrain
{
    public class TerrainGenerator : MonoBehaviour
    {
        //public int Seed = 0;
        public float MaxHeight{ get { return _height + _artifactsHeight; } } 

        [SerializeField]
        private float _height =  100f;    
        [SerializeField, Range(0.001f,0.5f)]
        private float _perlinScale = 0.05f;


        [SerializeField, Range(0.1f,100)]
        private float _artifactsHeight =  5f;
        [SerializeField, Range(0.01f,1f)]
        private float _artifactPerlinScale = 0.01f;

        [SerializeField, Range(0,5)]
        private float _slopeFactor = 2f;

        public const float BLOCK_SIZE = 1f;
        public static readonly Vector3 BLOCK_EXTENTS = new Vector3(BLOCK_SIZE * 0.5f, BLOCK_SIZE * 0.5f, BLOCK_SIZE * 0.5f);

        [SerializeField]
        private List<BlockPrototype> _blockPrototypes;

        [SerializeField]
        private AnimationCurve _terrainTypeByHeightCurve = new AnimationCurve(new Keyframe(0,0), new Keyframe(1,0.999f));

        private List<GameObject> _terrainChunkObjs = new List<GameObject>();

        public int BlockCount
        {
            get
            {
                int blockCount = 0;
                foreach (var chunk in _terrainChunkObjs)
                {
                    if (chunk!= null)
                    {
                        blockCount+=chunk.transform.childCount;
                    }
                }
                return blockCount;
            }
        }

        public void ClearTerrain()
        {
            /*
            for(int childCount = transform.childCount; childCount > 0; childCount--)
            {
                DestroyImmediate(transform.GetChild(childCount-1).gameObject);
            }
            */        

            foreach(var chunk in _terrainChunkObjs)
            {
                if(chunk == null)
                    continue;

                if(Application.isPlaying)
                {
                    Destroy(chunk);
                }
                else
                {
                    DestroyImmediate(chunk);
                }
            }
            _terrainChunkObjs.Clear();
        }

        public void RemoveChunk(GameObject terrainChunk)
        {
            _terrainChunkObjs.Remove(terrainChunk);
        }

        public GameObject GenerateArea(SerializableBounds area, TerrainChunk.TerrainChunkData chunkData = null, TerrainChunk chunk = null)
        {
            float startX = Mathf.CeilToInt(area.min.x);
            float endX = area.max.x;
            
            float startZ = Mathf.CeilToInt(area.min.z);
            float endZ = area.max.z;

            Vector3 currentPos = area.min;
            float baseTerrainHeight = 0;
            int terrainTypeCount = _blockPrototypes.Count;

            GameObject terrainChunkObj = new GameObject("TerrainChunk" + area.center.ToString());
            terrainChunkObj.layer = PhysicsLayers.TerrainLayer();
            Transform terrainChunkTransform = terrainChunkObj.transform;
            terrainChunkTransform.position = area.center;
            terrainChunkTransform.rotation = Quaternion.identity;            
            _terrainChunkObjs.Add(terrainChunkObj);

            for(currentPos.x = startX; currentPos.x < endX; currentPos.x += BLOCK_SIZE)
            {
                for(currentPos.z = startZ; currentPos.z < endZ; currentPos.z += BLOCK_SIZE)
                {
                    //Create column so there wont be holes in the terrain
                    float startingHeight = FindMinNeighborHeight(currentPos) + BLOCK_SIZE;
                    float blockHeight = SampleTerrainHeight(currentPos, out baseTerrainHeight);
                    if(startingHeight > blockHeight)
                        startingHeight = blockHeight;
                    
                    //Select terrain type by height
                    int prototypeIndex = Mathf.FloorToInt(terrainTypeCount * _terrainTypeByHeightCurve.Evaluate(Mathf.Clamp01(baseTerrainHeight/_height)));
                    BlockPrototype blockData = _blockPrototypes[prototypeIndex];

                    Vector3 spawnPos = new Vector3 (currentPos.x, startingHeight, currentPos.z);
                    for(; spawnPos.y <= blockHeight ; spawnPos.y += BLOCK_SIZE)
                    {
                        if (chunkData != null)
                        {
                            var removedBlocksMap = chunkData.GetBlockBitMapAtPos(spawnPos);
                            if (removedBlocksMap != null)
                            {
                                if (removedBlocksMap.WasBlockRemovedFromPosition(spawnPos))
                                    continue;
                            }
                        }
                        
                        //Check if there are no overlapping blocks
                        
                        Collider[] overlap = Physics.OverlapSphere(spawnPos, 0.2f, ~PhysicsLayers.TerrainLayer());
                        if(overlap.Length > 0)
                        {
                            Debug.LogError("Tried to spawn block at occupied position");
                            continue;
                        }
                        
                        //TODO optimization -> block pooling
                        GameObject newBlock = Instantiate(blockData.blockPrefab, spawnPos, Quaternion.identity, terrainChunkTransform) as GameObject;
                        var blockComponent = newBlock.GetComponent<Block>();
                        blockComponent.Init(blockData);
                    }
                }   
            }

            if (chunkData != null)
            {
                if (chunkData.AdditionalBlocksSerialized != null)
                {
                    foreach (var block in chunkData.AdditionalBlocksSerialized)
                    {
                        var blockProto = _blockPrototypes.Find((x) => x.GetProtoId() == block.BlockProtoId);
                        chunk.LoadCustomBlock(blockProto, block, terrainChunkObj);
                    }
                }
            }

            return terrainChunkObj;
        }
        /*
        public Block AddBlockAtPosition(Vector3 spawnPosition, Transform chunkTransform)
        {
            //TODO optimization -> block pooling
            BlockPrototype blockData = ChooseBlockPrototypeByHeightCurve(spawnPosition.y);
            GameObject newBlock = Instantiate(blockData.blockPrefab, spawnPosition, Quaternion.identity, chunkTransform) as GameObject;
            var blockComponent = newBlock.GetComponent<Block>();
            blockComponent.Init(blockData);
            return blockComponent;
        }
        */
        public BlockPrototype ChooseBlockPrototypeByHeightCurve(float baseTerrainHeight)
        {
            int prototypeIndex = Mathf.FloorToInt(_blockPrototypes.Count * _terrainTypeByHeightCurve.Evaluate(Mathf.Clamp01(baseTerrainHeight/_height)));
            return _blockPrototypes[prototypeIndex];
        }

        //Sample 5 horizontal positions in shape of a plus '+'
        private float FindMinNeighborHeight(Vector3 atPosition)
        {
            Vector3 currentPos = new Vector3(atPosition.x-BLOCK_SIZE, atPosition.y, atPosition.z);

            float baseTerrainHeight = 0;

            currentPos.z = atPosition.z;      
            float blockHeight = SampleTerrainHeight(currentPos, out baseTerrainHeight);

            float minHeight = blockHeight;
            
            currentPos.x += BLOCK_SIZE;      
            float endZ = atPosition.z + BLOCK_SIZE*2;
            for(currentPos.z =  atPosition.z - BLOCK_SIZE; currentPos.z < endZ; currentPos.z += BLOCK_SIZE)
            {
                blockHeight = SampleTerrainHeight(currentPos, out baseTerrainHeight);

                if(blockHeight < minHeight)
                    minHeight = blockHeight;
            }

            currentPos.x += BLOCK_SIZE;
            currentPos.z = atPosition.z;
            blockHeight = SampleTerrainHeight(currentPos, out baseTerrainHeight);

            if(blockHeight < minHeight)
                minHeight = blockHeight;

            return minHeight;
        }

        public float SampleTerrainHeight(Vector3 position, out float baseTerrainHeight)
        {
            //basic height
            float targetHeight = Mathf.PerlinNoise(position.x * _perlinScale, position.z * _perlinScale);     
            //Slope - Clamp to prevent outputting NaN
            targetHeight = Mathf.Pow(Mathf.Clamp( targetHeight,0.001f, float.MaxValue), _slopeFactor);        
            //artifacts
            float artifactHeight = Mathf.PerlinNoise(position.x * _artifactPerlinScale, position.z * _artifactPerlinScale);        

            //Artifacts in higher lands        
            artifactHeight = artifactHeight * targetHeight * _artifactsHeight;
            
            //Convert to world height and aling with grid
            targetHeight *= _height;
            baseTerrainHeight = targetHeight;

            targetHeight += artifactHeight;
            targetHeight -= targetHeight % BLOCK_SIZE;
            return targetHeight;
        }

    }
}