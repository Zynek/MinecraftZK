﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameSystem;
using Serialization;
using Object = UnityEngine.Object;

namespace Game.Terrain
{
    ///<summary>
    /// TerrainChunk manages save/load/dispose of a part of terrain. Also all kinds of terrain modification - digging / construction and holds info about these modifications.
    ///</summary>
    public class TerrainChunk
    {
        public const int CHUNK_SIZE = TerrainChunkMap.CHUNK_SIZE_INT;
        ///<summary>
        /// RemovedBlocksBitMap holds info about what blocks were removed from the terrain chunk. Each bit in RemovedBlocks bit array represents whether block is removed on the position in Bounds.
        /// BitArray3D has to be quite big to compensate for the memory cost of bounds and its own variables (dimension size).
        ///</summary>
        [Serializable]
        public class RemovedBlocksBitMap
        {
            public const int BIT_MAP_HEIGHT = TerrainChunkMap.CHUNK_SIZE_INT*8;
            //True if block was removed
            public BitArray3D RemovedBlocks;
            public SerializableBounds Bounds;

            public RemovedBlocksBitMap(Vector3 centerBlockPosition)
            {
                //Vertically store 5 times more the CHUNK_SIZE for better ratio between effectively used memory (size of BitArray) and it's management (Bounds, BitArray3D variables, pointers, etc.)                
                Vector3 size = new Vector3(CHUNK_SIZE, BIT_MAP_HEIGHT, CHUNK_SIZE);

                RemovedBlocks = new BitArray3D(CHUNK_SIZE, BIT_MAP_HEIGHT, CHUNK_SIZE);
                Bounds = new SerializableBounds(centerBlockPosition, size);
            }
            
            public bool WasBlockRemovedFromPosition(Vector3 fieldPosition)
            {
                Vector3Int fieldIndex = Vector3Int.FloorToInt(fieldPosition - Bounds.min);
                if(RemovedBlocks.IsInBounds(fieldIndex))
                {
                    return RemovedBlocks[fieldIndex];
                }
                
                throw new Exception("Out of Bounds:" + fieldPosition);
            }
        }

        public const float BLOCK_SIZE = TerrainGenerator.BLOCK_SIZE;
        public static readonly Vector3 BLOCK_V3 = new Vector3(BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
        public bool Loaded = false;
        //GameObject which is parent of all blocks contained in this TerrainChunk
        public GameObject ChunkObj = null;

        [Serializable]
        public class TerrainChunkData
        {         
            public SerializableBounds Bounds;        
            //Blocks added by player
            [NonSerialized]
            public List<Block> AdditionalBlocks = null;
            public List<SerializedBlock> AdditionalBlocksSerialized = null;
            //Blocks removed by player
            public List<RemovedBlocksBitMap> RemovedBlockMaps = null;

            public static Hash128 PositionToHash(Vector3 chunkCenter)
            {
                int X = Mathf.RoundToInt(chunkCenter.x) / CHUNK_SIZE;
                int Z = Mathf.RoundToInt(chunkCenter.z) / CHUNK_SIZE;

                return Hash128.Compute(X.ToString() + Z.ToString());
            }
            
            public RemovedBlocksBitMap GetBlockBitMapAtPos(Vector3 worldPosition)
            {
                if(RemovedBlockMaps != null)
                {
                    return RemovedBlockMaps.Find(
                        (x)=> {                                            
                            return x.Bounds.Contains(worldPosition);
                        }
                    ); 
                }
                return null;
            }
        }
        
        //Blocks added by player
        //public List<Block> AdditionalBlocks = null;

        public TerrainChunkData Data;
        private TerrainGenerator _generator = null;
        private TerrainChunkMap _parent;
        public TerrainChunkMap Terrain { get{ return _parent;} }

        private static readonly List<Vector3Int> _gapPositions = new List<Vector3Int>{
            new Vector3Int(0,1,1),
            new Vector3Int(2,1,1),
            new Vector3Int(1,0,1),
            new Vector3Int(1,2,1),
            new Vector3Int(1,1,0),
            new Vector3Int(1,1,2)
         };

        public TerrainChunk(SerializableBounds bounds, TerrainGenerator terrainGenerator, TerrainChunkMap parent)
        {
            Data = new TerrainChunkData();
            Data.Bounds = bounds;
            _generator = terrainGenerator;
            _parent = parent;
        }

        public void Load()
        {
            if(!Loaded)
            {
                //TODO load modifications from a file
                ChunkObj = _generator.GenerateArea(Data.Bounds, Data, this);
                var chunkComponent = ChunkObj.AddComponent(typeof(TerrainChunkComponent)) as TerrainChunkComponent;
                chunkComponent.ChunkLink = this;
                
                Loaded = true;
            }
        }

        public void AddCustomBlock(BlockPrototype chosenBlock, Vector3 position)
        {
            if(Data.AdditionalBlocks == null)
            {
                Data.AdditionalBlocks = new List<Block>();
            }

            var newBlockObj = Object.Instantiate(chosenBlock.blockPrefab, position, Quaternion.identity, ChunkObj.transform);
            var newBlock = newBlockObj.GetComponent<Block>();
            newBlock.Init(chosenBlock);
            Data.AdditionalBlocks.Add(newBlock);
        }
        
        public void LoadCustomBlock(BlockPrototype chosenBlock, SerializedBlock serializedBlock, GameObject parent)
        {
            if(Data.AdditionalBlocks == null)
            {
                Data.AdditionalBlocks = new List<Block>();
            }

            var newBlockObj = Object.Instantiate(chosenBlock.blockPrefab, serializedBlock.Position, Quaternion.identity, parent.transform);
            var newBlock = newBlockObj.GetComponent<Block>();
            newBlock.LoadBlock(chosenBlock, serializedBlock);
            Data.AdditionalBlocks.Add(newBlock);
        }

        public TerrainChunkData Unload()
        {
            if(Loaded)
            {
                Loaded = false;
                _generator.RemoveChunk(ChunkObj);
                //TODO optimization -> block pooling
                UnityEngine.Object.Destroy(ChunkObj);

                return GetChunkModifications();
            }

            return null;
        }

        public bool IsTerrainChunkModified()
        {
            return Data.RemovedBlockMaps != null || Data.AdditionalBlocks != null;
        }

        public TerrainChunkData GetChunkModifications()
        {
            if(IsTerrainChunkModified())
            {
                if(Data.AdditionalBlocksSerialized == null)
                    Data.AdditionalBlocksSerialized = new List<SerializedBlock>();
                
                if (Data.AdditionalBlocks != null)
                {
                    Data.AdditionalBlocksSerialized.Clear();
                    foreach (var block in Data.AdditionalBlocks)
                    {
                        if (block != null)
                            Data.AdditionalBlocksSerialized.Add(block.Serialize());
                    }
                }

                return Data;
            }

            return default;
        }

        internal bool OnBlockCrushed(Vector3 blockPos, Block block)
        {
            if(!Data.Bounds.Contains(blockPos))
            {
                Debug.LogError("Block destruction denied !");
                return false;
            }

            //Get all neighbor blocks
            Collider[] neighbors = Physics.OverlapBox(blockPos,Vector3.one, Quaternion.identity, PhysicsLayers.GetTerrainLayerMask(), QueryTriggerInteraction.Ignore);
            //Debug.Log("Neighbor count:" + neighbors.Length);

            float[,] localHeightMap = SampleTerrain3x3(blockPos);

            //Sample neighbors to bit array
            BitArray3D occupied = new BitArray3D(3,3,3);
            
            foreach(var col in neighbors )
            {
                Vector3Int offset = Vector3Int.FloorToInt( col.transform.position - blockPos );
                occupied[1+offset.x, 1+offset.y, 1+offset.z] = true;
            }

            if( Data.RemovedBlockMaps== null)
                Data.RemovedBlockMaps = new List<RemovedBlocksBitMap>();

            RemovedBlocksBitMap map = GetBlockBitMapAtPos(blockPos); 
            
            //Fill in the gaps
            foreach(var gapPosition in _gapPositions)
            {
                int x = gapPosition.x, y = gapPosition.y, z = gapPosition.z;
                if(!occupied[x, y, z])
                {
                    Vector3 fieldPosition = blockPos + new Vector3(x-1,y-1,z-1);
                    if(fieldPosition.y < localHeightMap[x,z])
                    {
                        TerrainChunk relatedChunk = this;
                        RemovedBlocksBitMap relatedBitMap = map;
                        
                        if(!Data.Bounds.Contains(fieldPosition))
                        {
                            //Handle case where field position is out of the TerrainChunkBounds
                            relatedChunk = _parent.GetChunkAtWorldPosition(fieldPosition);           
                            
                            if(relatedChunk == this)
                            {
                                throw new ArgumentException("This new block is supposed to be in different chunk, but GetChunkAtWorldPosition returned the SAME CHUNK");
                            }
                        }   
                             
                        relatedBitMap =  relatedChunk.GetBlockBitMapAtPos(fieldPosition);

                        if(relatedBitMap != null)
                        {
                            //Don't create blocks where they were already removed
                            if (relatedBitMap.WasBlockRemovedFromPosition(fieldPosition))
                                continue;
                        }
                        relatedChunk.AddCustomBlock(_generator.ChooseBlockPrototypeByHeightCurve(fieldPosition.y),fieldPosition);                                          
                    }
                }
            }
            
            if(map == null)
            {
                Vector3 mapCenter = Data.Bounds.center;
                mapCenter.y = blockPos.y;
                float modulo = (blockPos.y % RemovedBlocksBitMap.BIT_MAP_HEIGHT);
                if(modulo < 0f)
                {
                    modulo *= -1;
                    modulo = RemovedBlocksBitMap.BIT_MAP_HEIGHT - modulo;
                }

                mapCenter.y -= modulo;
                mapCenter.y += RemovedBlocksBitMap.BIT_MAP_HEIGHT/2;
                mapCenter.y -= TerrainGenerator.BLOCK_EXTENTS.y;
                map = new RemovedBlocksBitMap(mapCenter);

                //Debug.Log("Created BitMap at height:" + mapCenter.y + " blockPos:" + blockPos);
                Data.RemovedBlockMaps.Add(map);
            }
            Vector3Int roundedPos = Vector3Int.FloorToInt(blockPos - map.Bounds.min);
            try
            {
                map.RemovedBlocks[roundedPos] = true;
            }
            catch
            {
                Debug.LogError("mapCount:" + Data.RemovedBlockMaps.Count);
                throw;
            }

            Data.AdditionalBlocks?.Remove(block);

            return true;
        }

        //It would be nice to have more optimized solution, but as long as it is triggered only few dozen times a second - optimization would not be very beneficial comapared to coding time required.
        internal RemovedBlocksBitMap GetBlockBitMapAtPos(Vector3 worldPosition)
        {
            if(Data.RemovedBlockMaps != null)
            {
                return Data.RemovedBlockMaps.Find(
                    (x)=> {                                            
                        return x.Bounds.Contains(worldPosition);
                    }
                ); 
            }
            return null;
        }

        private float[,] SampleTerrain3x3(Vector3 blockPos)
        {
            //Find out original height of 3x3 grid
            Vector3 start = blockPos - BLOCK_V3;
            Vector3 end = blockPos + BLOCK_V3;

            Vector3 currentPos = new Vector3(start.x, start.y,start.z);
            float baseTerrainHeight = 0f;
            //We will use this to avoid generating blocks above terrain
            float[,] localHeightMap = new float[3,3];

            for(int i = 0; currentPos.x <= end.x; currentPos.x += BLOCK_SIZE, i++)
            {
                currentPos.z = start.z;
                for(int j = 0; currentPos.z <= end.z; currentPos.z += BLOCK_SIZE, j++)
                {
                    float blockHeight = _generator.SampleTerrainHeight(currentPos, out baseTerrainHeight);
                    localHeightMap[i,j] = blockHeight;
                }
            }

            return localHeightMap;
        }

    }

    ///<summary>
    /// This component is added to ChunkObj of TerrainChunk to create bridge between blocks and TerrainChunk
    ///</summary>
    public class TerrainChunkComponent: MonoBehaviour
    {
        public TerrainChunk ChunkLink = null;

        #if UNITY_EDITOR
        //Helps to visualize changes in terrain inside the scene window
        void OnDrawGizmos()
        {
            if(ChunkLink != null)
            {
                if(ChunkLink.Data.RemovedBlockMaps != null)
                {
                    Gizmos.color = Color.yellow;
                    Gizmos.DrawWireCube(ChunkLink.Data.Bounds.center, ChunkLink.Data.Bounds.size);

                    Gizmos.color = Color.cyan;
                    foreach(var map in ChunkLink.Data.RemovedBlockMaps)
                    {
                        Gizmos.DrawWireCube(map.Bounds.center, map.Bounds.size);
                    }
                }
            }
        }
        #endif
    }
}