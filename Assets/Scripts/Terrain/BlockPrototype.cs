﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BlockProto1", menuName = "WorldData/Block", order = 1)]
public class BlockPrototype : ScriptableObject
{
    public GameObject blockPrefab = (GameObject)default;
    public int Hp = 1;     
    public GameObject VFX_DestructionPrefab = (GameObject)default;
    public AudioCollection FootstepsSFX = default;
        
    
    //Requires unique Id across all block prototypes
    [SerializeField]
    private short _id;
    
    public short GetProtoId()
    {
        return _id;
    }    
}