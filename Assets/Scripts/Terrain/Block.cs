﻿using System;
using UnityEngine;

namespace Game.Terrain
{
    public class Block : MonoBehaviour
    {
        BlockPrototype _blockData = null;
        private int _hp = 1;
        public AudioCollection FootstepSoundCollection => _blockData.FootstepsSFX;

        public void Init(BlockPrototype blockData)
        {
            _hp = blockData.Hp;
            _blockData = blockData;
        }

        public void LoadBlock(BlockPrototype blockData, SerializedBlock serializedBlock)
        {
            _hp = serializedBlock.Hp;
            _blockData = blockData;
        }

        public void TakeDamage(int dmg)
        {
            _hp -= dmg;
            if(_hp < 1)
            {
                Crush();
            }
        }

        public SerializedBlock Serialize()
        {
            return new SerializedBlock{ Hp = _hp, BlockProtoId = _blockData.GetProtoId(), Position = transform.position };
        }
        /*
        public void Deserialize(SerializedBlock block)
        {
            _hp = block.Hp;
        }
        */
        private void Crush()
        {
            Vector3 position = transform.position;
            TerrainChunkComponent parentChunk = transform.parent.GetComponent<TerrainChunkComponent>();
            if(parentChunk.ChunkLink.OnBlockCrushed(position, this))
            {        
                Instantiate(_blockData.VFX_DestructionPrefab, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
        }
    }
    
    [Serializable]
    public class SerializedBlock
    {
        public short BlockProtoId = 0;
        public int Hp = 1;
        public SerializableVector3 Position;
    }
}