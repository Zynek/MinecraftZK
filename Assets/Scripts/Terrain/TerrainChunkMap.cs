﻿using System.Collections.Generic;
using Serialization;
using UnityEngine;

namespace Game.Terrain
{
    ///<summary>
    /// Determines which terrain chunks should be loaded/unloaded based on given position and view distance.
    /// This is done by keeping 2D array of TerrainChunks which represents loaded chunks. When player moves the chunks are moved in the array accordingly 
    ///</summary>
    public class TerrainChunkMap
    {
        //Must be even number otherwise Bounds won't work correctly and there will be overlapping blocks
        public const int CHUNK_SIZE_INT = 4;
        public const float BASIC_CHUNK_SIZE = TerrainGenerator.BLOCK_SIZE * CHUNK_SIZE_INT;
        public const float BLOCK_SIZE = TerrainGenerator.BLOCK_SIZE;
        public const float MAX_MINING_DEPTH = 10000 * BLOCK_SIZE;

        public Dictionary<Hash128, TerrainChunk.TerrainChunkData> ModifiedTerrainChunks;

        private float _terrainChunkSize = BASIC_CHUNK_SIZE, _viewDistance = 10f, _unloadDistance = 15f;
        private float _viewDistSqr, _unloadDistSqr, _maxTerrainHeight;
        
        private Vector3 _originPos = Vector3.zero;
        private TerrainChunk[,] _chunkMap = null;
        private int _gridSize = 0;
        private int _centerIndex = 0;//Should always be _gridSize/2

        private TerrainGenerator _terrainGenerator = null;

        public TerrainChunkMap(Vector3 startingPos, float viewDistance, TerrainGenerator terrainGenerator)
        {        
            _terrainChunkSize = BASIC_CHUNK_SIZE;
            
            _originPos = startingPos;
            _terrainGenerator = terrainGenerator;
            _maxTerrainHeight = terrainGenerator.MaxHeight;

            ModifiedTerrainChunks = new Dictionary<Hash128, TerrainChunk.TerrainChunkData>();

            SetViewDistance(viewDistance);
        }
        
        public float ViewDistance{ get { return _viewDistance; } }
        public void SetViewDistance(float distance)
        {
            distance = Mathf.Clamp(distance, BASIC_CHUNK_SIZE, float.MaxValue);
            _viewDistance = distance;
            _viewDistSqr = _viewDistance * _viewDistance;

            _unloadDistance = _viewDistance * 1.3f;
            _unloadDistSqr = _unloadDistance * _unloadDistance;

            int checkSize = CalcChunkMapSize(_unloadDistance);
            if(checkSize != _gridSize)
            {
                ResizeChunkMap();
            }
        }
        
        private void ResizeChunkMap()
        {
            int newSize = CalcChunkMapSize(_unloadDistance);
            int newCenterIndex = newSize/2;
            TerrainChunk[,] newChunkMap = new TerrainChunk[newSize,newSize];

            if(_chunkMap != null)
            {
                int centerDiff = newCenterIndex - _centerIndex;
                Vector2Int projection = new Vector2Int(0,0);
                for(int x = 0; x < _gridSize; x++ )
                {
                    for(int z = 0; z < _gridSize; z++)
                    {
                        projection.x = x + centerDiff;
                        projection.y = z + centerDiff;
                        
                        if(IsInChunkMapBounds(projection, newSize))
                        {
                            newChunkMap[projection.x,projection.y] = _chunkMap[x,z];
                        }
                        else
                        {
                            _chunkMap[x,z].Unload();
                        }
                    }
                }
            }

            _gridSize = newSize;
            _centerIndex = newCenterIndex;
            _chunkMap = newChunkMap;
        }
        
        public void Update(Vector3 viewCenter)
        {
            //Limit to 2D for now
            viewCenter.y = _originPos.y;

            CenterChunkMap(viewCenter);

            //Evaluate terrain chunk distance from player and load / unload chunks
            for(int x = 0; x < _gridSize; x++)
            {
                for(int z = 0; z < _gridSize; z++)
                {
                    TerrainChunk theChunk = _chunkMap[x,z];
                    if(theChunk == null)
                    {
                        Vector3 chunkPos = GridToWorldCoord(x,_centerIndex,z);
                        Hash128 chunkKey = TerrainChunk.TerrainChunkData.PositionToHash(chunkPos);
                        theChunk = new TerrainChunk( CreateChunkBounds( chunkPos ) , _terrainGenerator, this );
                        _chunkMap[x, z] = theChunk;
                        if(ModifiedTerrainChunks.ContainsKey(chunkKey))
                        {
                            theChunk.Data = ModifiedTerrainChunks[chunkKey];
                        }
                    }
                    
                    Vector3 dir = theChunk.Data.Bounds.center - viewCenter;
                    float distanceSqr = dir.sqrMagnitude;

                    if(distanceSqr > _unloadDistSqr)
                    {
                        var chunkData = theChunk.Unload();
                        if(chunkData != null)                        
                        {
                            ModifiedTerrainChunks[TerrainChunk.TerrainChunkData.PositionToHash(chunkData.Bounds.center)] = chunkData;
                        }
                    }
                    else if(distanceSqr < _viewDistSqr)
                    {
                        theChunk.Load();
                    }
                }
            }

        }

        public Dictionary<Hash128, TerrainChunk.TerrainChunkData> CollectTerrainModifications()
        {
            foreach (var terrainChunk in _chunkMap)
            {
                var mods = terrainChunk.GetChunkModifications();
                if (mods != null)
                {
                    ModifiedTerrainChunks[TerrainChunk.TerrainChunkData.PositionToHash(mods.Bounds.center)] = mods;
                }
            }

            return ModifiedTerrainChunks;
        }

        public void UnloadTerrain()
        {
            for(int x = 0; x < _gridSize; x++)
            {
                for(int z = 0; z < _gridSize; z++)
                {
                    TerrainChunk theChunk = _chunkMap[x,z];
                    if(theChunk != null)
                    {
                        theChunk.Unload();
                    }

                    _chunkMap[x, z] = null;
                }
            }
            
            ModifiedTerrainChunks.Clear();
        }

        ///<summary>
        /// Shifts the whole chunk map matrix so it is centered near <paramref name="viewCenter"/>
        ///</summary>
        private void CenterChunkMap(Vector3 viewCenter)
        {            
            Vector3 viewOffset = viewCenter - _originPos;
            if(viewOffset.sqrMagnitude > _terrainChunkSize*_terrainChunkSize*2)
            {
                //Move grid cells
                Vector3 newOrigin = ChunkCenter(viewCenter);
                viewOffset = newOrigin - _originPos;
                Vector2Int offset = new Vector2Int(Mathf.RoundToInt(viewOffset.x/_terrainChunkSize), Mathf.RoundToInt(viewOffset.z/_terrainChunkSize));
                //Debug.Log("CenterChunkMap: viewCenter:" + viewCenter + " / origin:" + _originPos + " / newOrigin:" + newOrigin + " / offset:" + offset);
                
                Vector2Int gridPos = new Vector2Int(0,0);
                Vector2Int gridPosOffset = new Vector2Int(0,0);
                //TODO optimize - use existing chunk map and avoid additional allocations
                TerrainChunk[,] newChunkMap = new TerrainChunk[_gridSize,_gridSize];
                                
                for(int x = 0; x < _gridSize; x++)
                {
                    for(int z = 0; z < _gridSize; z++)
                    {
                        gridPos.Set(x,z);                        
                        gridPosOffset = gridPos - offset;

                        if(IsInChunkMapBounds(gridPos) && IsInChunkMapBounds(gridPosOffset))
                        {
                            newChunkMap[gridPosOffset.x, gridPosOffset.y] = _chunkMap[x,z];                            
                            _chunkMap[x,z] = null;
                        }
                    }
                }
                foreach (var chunk in _chunkMap)
                {
                    if(chunk != null)
                    {
                        chunk.Unload();
                    }
                }

                _chunkMap = newChunkMap;
                _originPos = newOrigin;
            }
        }
        
        private int CalcChunkMapSize(float unloadDistance )
        {
            int size = 2 + Mathf.CeilToInt(unloadDistance / _terrainChunkSize);
            if((size %2) ==0)
            {
                size++;
            }
            return size;
        }

        ///<summary>
        /// Create bounds for one terrain chunk near <paramref name="position"/> aligned to chunk grid
        ///</summary>
        public SerializableBounds CreateChunkBounds(Vector3 position)
        {
            return new SerializableBounds(ChunkCenter(position) - TerrainGenerator.BLOCK_EXTENTS, new Vector3(_terrainChunkSize ,MAX_MINING_DEPTH*2f,_terrainChunkSize));         
        }

        ///<summary>
        /// Snap to chunk grind        
        ///</summary>
        public Vector3 ChunkCenter(Vector3 position)
        {
            Vector3 center = new Vector3(
                position.x - position.x % _terrainChunkSize,
                position.y - position.y % _terrainChunkSize,
                position.z - position.z % _terrainChunkSize
            );

            return center;
        }

        public Vector3 GridToWorldCoord(int x, int y, int z)
        {
            return new Vector3(
                            (x-_centerIndex) * _terrainChunkSize + _originPos.x, 
                            (y-_centerIndex) * _terrainChunkSize + _originPos.y,
                            (z-_centerIndex) * _terrainChunkSize + _originPos.z
                        );
        }

        public TerrainChunk GetChunkAtWorldPosition(Vector3 worldPosition)
        {
            Vector3 localOffset = worldPosition - _originPos + TerrainGenerator.BLOCK_EXTENTS;
            localOffset /= CHUNK_SIZE_INT;
            Vector2Int gridPos = new Vector2Int( Mathf.RoundToInt(localOffset.x) + _centerIndex, Mathf.RoundToInt(localOffset.z) + _centerIndex);
            if(IsInChunkMapBounds(gridPos))
            {
                return _chunkMap[gridPos.x, gridPos.y];
            }
            return null;             
        }

        private bool IsInChunkMapBounds(Vector2Int gridPos)
        {
            return gridPos.x > -1 && gridPos.y > -1 && gridPos.x < _gridSize && gridPos.y < _gridSize; 
        }

        private bool IsInChunkMapBounds(Vector2Int gridPos, int boundsSize)
        {
            return gridPos.x > -1 && gridPos.y > -1 && gridPos.x < boundsSize && gridPos.y < boundsSize; 
        }

    }
}