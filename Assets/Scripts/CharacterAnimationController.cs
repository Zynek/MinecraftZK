﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game.Characters;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Character))]
public class CharacterAnimationController : MonoBehaviour
{
    Animator _characterAnim;
    Character _character;
    void Awake()
    {
        _characterAnim = GetComponent<Animator>();
        
        _character = GetComponent<Character>();
        _character.OnCharModeAction += OnCharModeAction;
    }
    
    private void OnCharModeAction()
    {
        _characterAnim.SetTrigger("Swing");
    }

    private void OnDestroy()
    {
        if(_character != null)
            _character.OnCharModeAction -= OnCharModeAction;
    }
}
