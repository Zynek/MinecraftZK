﻿using System;
using Game.Terrain;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Characters
{   
    ///<summary>
    /// Common class for all characters. Expected to be controlled by one of CharController.
    ///</summary>
    [RequireComponent(typeof (Rigidbody))]
    [RequireComponent(typeof (CapsuleCollider))]
    public class Character : MonoBehaviour
    {
        #region Settings
        [Serializable]
        public class MovementSettings
        {
            public float MovementSpeed = 8.0f;   // Speed when walking forward
            public float Drag = 20.0f;   // Speed when walking forward
            public float JumpForce = 30f;
            
            public float GroundCheckDistance = 0.01f; // distance for checking if the controller is grounded ( 0.01f seems to work best for this )
            public float StickToGroundHelperDistance = 0.5f; // stops the character
            [HideInInspector] public float CurrentTargetSpeed = 8f;

            public void UpdateDesiredTargetSpeed(Vector2 input)
            {
	            if (input == Vector2.zero)
                { 
                    CurrentTargetSpeed = 0f;
                    return;
                }
				CurrentTargetSpeed = MovementSpeed;				                
            }
        }

        #endregion //Settings

        public Action OnCharModeAction;

        public void TriggerCharacterModeAction()
        {
            OnCharModeAction?.Invoke();
        }

        public MovementSettings _movementSettings = new MovementSettings();
        
        private Rigidbody _rigidBody;
        private CapsuleCollider _capsule;
        private Vector3 _groundContactNormal;
        private bool _jump, _previouslyGrounded, _jumping, _isGrounded;        
        private Vector3 _desiredMovement;
        private float _oldYRotation;
        private Ray _pickingRay;
        private bool _controlledByGamepad;

        [SerializeField] private AudioSource _footstepAudioSource = default;
        [SerializeField] private AudioSource _itemAudioSource = default;
        public AudioSource ItemAudioSource => _itemAudioSource;

        //This could be a temporary variable, but this way we can see it in the inspector in debug mode
        private float _desiredMagnitude = 0f;
        private Block _standingOnBlock = default;

        private void Start()
        {
            _rigidBody = GetComponent<Rigidbody>();
            _capsule = GetComponent<CapsuleCollider>();
            
            _rigidBody.drag = 0f;
        }


        public void SetPickingRay(Ray newRay)
        {
            _pickingRay = newRay;
        }

        public Ray GetPickingRay()
        {
            return _pickingRay;
        }

        public void Jump()
        {
            if(!_jump)
            {
                _jump = true;
            }
        }

        public void UpdateMovementDirection(Vector3 desiredMovement, float oldYRotation, bool controlledByGamepad)
        {            
            _desiredMovement = desiredMovement; 
            _oldYRotation = oldYRotation;
            _controlledByGamepad = controlledByGamepad;
        }

        private void FixedUpdate()
        {
            GroundCheck();
            
            Vector3 desiredMove = Vector3.ProjectOnPlane(_desiredMovement, _groundContactNormal).normalized;
            
            desiredMove *= _movementSettings.CurrentTargetSpeed;
            _desiredMagnitude = _desiredMovement.magnitude;
            if (_controlledByGamepad)
            {
                desiredMove *= _desiredMagnitude;
            }
            Vector3 velocity = _rigidBody.velocity;
            desiredMove.y = velocity.y;
            _rigidBody.velocity = desiredMove;

            UpdateFootstepSound(_desiredMagnitude);
            
            if (_isGrounded)
            {
                /*
                if(_movementSettings.CurrentTargetSpeed > float.Epsilon)
                {
                    _rigidBody.drag = _movementSettings.Drag;
                }
                else
                {
                    //Slow down faster when player doesn't want to move
                    _rigidBody.drag = _movementSettings.Drag*2f;
                }
                */
                if (_jump)
                {
                    //_rigidBody.drag = 0f;
                    var newVelocity = _rigidBody.velocity;
                    newVelocity.y = 0f;
                    _rigidBody.velocity = newVelocity;
                    _rigidBody.AddForce(new Vector3(0f, _movementSettings.JumpForce, 0f), ForceMode.Impulse);
                    _jumping = true;
                }

                //Put rigidbody to sleep if not moving
                if (!_jumping && _movementSettings.CurrentTargetSpeed < float.Epsilon && _rigidBody.velocity.magnitude < 1f)
                {
                    _rigidBody.Sleep();
                }
            }
            else
            {
                //_rigidBody.drag = 0f;
                if (_previouslyGrounded && !_jumping)
                {
                    StickToGroundHelper();
                }
            }
            /*
            if(!_isGrounded)
            {            
                // Rotate the rigidbody velocity to match the new direction that the character is looking
                Quaternion velRotation = Quaternion.AngleAxis(transform.eulerAngles.y - _oldYRotation, Vector3.up);
                _rigidBody.velocity = velRotation*_rigidBody.velocity;
            }
            */
            _jump = false;        
        }

        private void StickToGroundHelper()
        {
            RaycastHit hitInfo;
            if (Physics.SphereCast(transform.position, _capsule.radius * 0.9f, Vector3.down, out hitInfo,
                                   ((_capsule.height/2f) - _capsule.radius) +
                                   _movementSettings.StickToGroundHelperDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore))
            {
                if (Mathf.Abs(Vector3.Angle(hitInfo.normal, Vector3.up)) < 85f)
                {
                    _rigidBody.velocity = Vector3.ProjectOnPlane(_rigidBody.velocity, hitInfo.normal);
                }
            }
        }

        /// sphere cast down just beyond the bottom of the capsule to see if the capsule is colliding round the bottom
        private void GroundCheck()
        {
            _previouslyGrounded = _isGrounded;
            RaycastHit hitInfo;
            if (Physics.SphereCast(transform.position + _capsule.center, _capsule.radius * 0.9f, Vector3.down, out hitInfo,
                                   ((_capsule.height/2f) - _capsule.radius) + _movementSettings.GroundCheckDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore))
            {
                _isGrounded = true;
                _groundContactNormal = hitInfo.normal;
                _standingOnBlock = hitInfo.collider.gameObject.GetComponent<Block>();
            }
            else
            {
                _isGrounded = false;
                _groundContactNormal = Vector3.up;
            }
            if (!_previouslyGrounded && _isGrounded && _jumping)
            {
                _jumping = false;
            }
        }

        private void UpdateFootstepSound(float movementSpeed)
        {
            if (movementSpeed > float.Epsilon)
            {
                if (!_footstepAudioSource.isPlaying && _isGrounded)
                {
                    _footstepAudioSource.clip = _standingOnBlock.FootstepSoundCollection.GetRandomClip();
                    _footstepAudioSource.Play();
                }
            }
            else
            {
                _footstepAudioSource.Stop();
            }
        }
    }


}