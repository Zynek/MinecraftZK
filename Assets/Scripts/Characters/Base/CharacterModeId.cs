﻿
namespace Game.Characters.Base
{
    public enum CharacterModeId
    {
            None = 0,
            Mining = 1,
            Builder = 2,
    }
}