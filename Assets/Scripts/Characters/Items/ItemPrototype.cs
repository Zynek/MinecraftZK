﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game.Characters.Base;

namespace Game.Characters.Items
{
    [CreateAssetMenu(fileName = "ItemProto1", menuName = "WorldData/Item", order = 1)]
    public class ItemPrototype : ScriptableObject
    {
        public GameObject ItemVisual = (GameObject)default;
        public CharacterModeId ActivatesMode = CharacterModeId.None;
        public GameObject ActivationVFX = (GameObject)default;
        public int AttackDmg = 0;
        public AudioCollection ItemSFX = default;
    }
}