﻿using UnityEngine;

using GameSystem;
using Game.Terrain;
using Game.Characters.Base;
using Game.Characters.Items;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;
using Debug = System.Diagnostics.Debug;

namespace Game.Characters
{
    ///<summary>
    /// MiningMode enables digging into terrain.
    /// Player can be both holding mouse button and clicking. Clicking makes digging faster. This improves player engagement. (Faster results for extra effort)
    ///</summary>
    public class MiningMode : CharacterMode
    {
        public const float MIN_HIT_DURATION = 0.3f;
        public const float LAZY_HIT_DURATION = 0.55f;
        
        private float _lastHitTime = float.MinValue;
        private bool _pressed = false;
        
        public MiningMode(Character character) : base(character)
        {
        }

        public override CharacterModeId ModeId(){ return CharacterModeId.Mining; }


        public override void Enable()
        {
            GameInputSingleton.Input.PC.UseHeldItemPrimary.performed += AttemptMining;
            GameInputSingleton.Input.PC.UseHeldItemPrimary.canceled += OnRelease;
        }

        public override void Disable()
        {
            GameInputSingleton.Input.PC.UseHeldItemPrimary.performed -= AttemptMining;
            GameInputSingleton.Input.PC.UseHeldItemPrimary.canceled -= OnRelease;
        }

        public override void Update()
        {
            /*
            if (Mouse.current.leftButton.wasReleasedThisFrame)
            {
                _pressed = false;
            }
            */
            if (_pressed)
            {
                AttemptMining(LAZY_HIT_DURATION);
            }
        }

        private void OnRelease(InputAction.CallbackContext obj)
        {
            _pressed = false;
        }
        
        private void AttemptMining(InputAction.CallbackContext obj)
        {
            _pressed = true;

            AttemptMining(MIN_HIT_DURATION);
        }
        
        private void AttemptMining(float hitDuration)
        {
            if (!(Time.time > _lastHitTime + hitDuration)) 
                return;
            
            Ray pickRay = _character.GetPickingRay();
            //Debug.DrawRay(pickRay.origin, pickRay.direction*5f, Color.yellow);
            if(Physics.Raycast(pickRay, out RaycastHit hit, 5f, PhysicsLayers.GetTerrainLayerMask(), QueryTriggerInteraction.Ignore))
            {
                Transform target = hit.transform;                    
                if( target != null)
                {   
                    target.gameObject.GetComponent<Block>()?.TakeDamage(_currentItem.AttackDmg);
                }

                if(_currentItem.ActivationVFX)
                {
                    GameObject.Instantiate(_currentItem.ActivationVFX, hit.point, Quaternion.identity);
                }
            }

            _character.TriggerCharacterModeAction();
            PlayItemSFX();
            
            _lastHitTime = Time.time;
        }

    }
}