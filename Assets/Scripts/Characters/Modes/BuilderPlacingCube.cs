﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Characters
{
    public class BuilderPlacingCube : MonoBehaviour
    {
        public Color PlacementEnabledColor;
        public Color PlacementDisabledColor;
        public Material CubeMaterial;

        private int _playerCollisionCount = 0;

        ///<summary>
        /// Returns true if no character is currently colliding with the placing cube.
        ///</summary>
        public bool PlacementEnabled => _playerCollisionCount < 1;

        void Start()
        {
            RefreshPlacementAvailability();
        }

        void OnTriggerEnter(Collider col)
        {
            if(col.GetComponent<Character>() != null)
            {
                _playerCollisionCount++;            
                RefreshPlacementAvailability();
            }
        }

        void OnTriggerExit(Collider col)
        {
            if(col.GetComponent<Character>() != null)
            {
                _playerCollisionCount--;            
                RefreshPlacementAvailability();
            }
        }

        private void RefreshPlacementAvailability()
        {
            CubeMaterial.color = PlacementEnabled ? PlacementEnabledColor : PlacementDisabledColor;
        }
    }
}