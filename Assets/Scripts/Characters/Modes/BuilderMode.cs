﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameSystem;
using Game.Terrain;
using Game.Characters.Base;
using UnityEngine.InputSystem;

namespace Game.Characters
{
    ///<summary>
    /// BuilderMode enables placing blocks on free spots next to another block. Has limited range.
    ///</summary>
    public class BuilderMode : CharacterMode
    {
        public BuilderMode(Character controller) : base(controller)
        {
        }

        public override CharacterModeId ModeId(){ return CharacterModeId.Builder; }

        private BuilderPlacingCube _placingCube;
        
        private List<BlockPrototype> _blockPrototypes = new List<BlockPrototype>();
        private BlockPrototype _chosenBlock = null;
        private Transform _target;
        public BlockPrototype ChosenBlock => _chosenBlock;

        public override void Enable()
        {
            if(_placingCube is null)
            {
                var cubeObj = GameObject.Instantiate(Resources.Load<GameObject>("Modes/PlacingCube"));
                _placingCube = cubeObj.GetComponent<BuilderPlacingCube>();
            }

            if(_blockPrototypes.Count == 0)
            {
                BlockPrototype[] prototypes = Resources.LoadAll<BlockPrototype>("WorldData/Blocks");
                foreach(var proto in prototypes)
                {
                    _blockPrototypes.Add(proto);
                }
                _chosenBlock = _blockPrototypes[0];
                //Debug.Log("Loaded block prototypes:" + _blockPrototypes.Count);
            }

            _placingCube.gameObject.SetActive(true);
            
            GameInputSingleton.Input.PC.UseHeldItemPrimary.performed += OnPlaceBlockAction;
            GameInputSingleton.Input.PC.NextCube.performed += OnUpdateBlockChoice;
        }

        public override void Disable()
        {
            _placingCube.gameObject.SetActive(false);
            
            GameInputSingleton.Input.PC.UseHeldItemPrimary.performed -= OnPlaceBlockAction;
            GameInputSingleton.Input.PC.NextCube.performed -= OnUpdateBlockChoice;
        }

        public override void Update()
        {
            if(_placingCube != null)
            {
                Ray pickRay = _character.GetPickingRay();
                
                //Debug.DrawRay(pickRay.origin, pickRay.direction*5f, Color.yellow);
                if(Physics.Raycast(pickRay, out RaycastHit hit, 5f, PhysicsLayers.GetTerrainLayerMask(), QueryTriggerInteraction.Ignore))
                {
                    _target = hit.transform;
                    if( _target != null && _target.gameObject.GetComponent<Block>() != null)
                    {                    
                        Vector3 hitPos = hit.point + hit.normal * 0.3f;
                        Vector3 rounded = new Vector3(Mathf.Round(hitPos.x),Mathf.Round(hitPos.y),Mathf.Round(hitPos.z));
                        _placingCube.transform.position = rounded;
                    }
                }
                else
                {
                    _placingCube.transform.position = hit.point;
                    _target = null;
                }
            }
        }

        private void OnPlaceBlockAction(InputAction.CallbackContext obj)
        {
            if(_placingCube.PlacementEnabled && _target != null)     
            {
                var chunkLink = _target.parent.GetComponent<TerrainChunkComponent>();
                var position = _placingCube.transform.position;
                var terrainChunk = chunkLink.ChunkLink.Terrain.GetChunkAtWorldPosition(position);
                terrainChunk.AddCustomBlock(_chosenBlock, position);   
                _character.TriggerCharacterModeAction();
                PlayItemSFX();
            }     
        }
        
        private void OnUpdateBlockChoice(InputAction.CallbackContext obj)
        {
            if (_placingCube != null)
            {
                int i = _blockPrototypes.IndexOf(_chosenBlock);
                i++;
                i %= _blockPrototypes.Count;
                _chosenBlock = _blockPrototypes[i];
            }
        }
    }
}