﻿using System;

using Game.Characters.Base;
using Game.Characters.Items;

namespace Game.Characters
{
    ///<summary>
    /// Base class for character modes. Should implement common functionality.
    ///</summary> 
    public abstract class CharacterMode
    {
        public static Action<CharacterMode> CharModeChanged;

        protected Character _character;
        protected ItemPrototype _currentItem;

        public CharacterMode(Character character)
        {
            _character = character;
        }

        public void SetItem(ItemPrototype itemPrototype)
        {
            _currentItem = itemPrototype;
        }

        public virtual CharacterModeId ModeId()
        {
            return CharacterModeId.None;
        }
        public abstract void Enable();

        public abstract void Disable();

        public abstract void Update();
        
        protected void PlayItemSFX()
        {
            _character.ItemAudioSource.clip = _currentItem.ItemSFX.GetRandomClip();
            _character.ItemAudioSource.Play();
        }
    }
}