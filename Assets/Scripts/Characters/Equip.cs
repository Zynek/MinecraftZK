﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game.Characters.Items;
using Game.Characters.Base;
using UnityEngine.InputSystem;

namespace Game.Characters
{
    public class Equip : MonoBehaviour
    {
        [SerializeField]
        private Character _character;

        [SerializeField]
        private Transform _rightHand;    
        private CharacterMode _activeMode = null;
        public CharacterMode ActiveMode
        {
            get{ return _activeMode;}
            set
            { 
                if(value != _activeMode)
                {
                    if(_activeMode != null)
                        _activeMode.Disable();

                    if(value != null)
                        value.Enable();

                    _activeMode = value;                    
                    CharacterMode.CharModeChanged?.Invoke(value);
                }
            }
        }
        private List<CharacterMode> _modes = null;
        private ItemPrototype _activeItem = null;

        public ItemPrototype ActiveItem 
        {
            get { return _activeItem; }
            set 
            { 
                for(int i = _rightHand.transform.childCount-1; i > -1; i--)
                {
                    Transform itemT = _rightHand.transform.GetChild(i);
                    if(itemT != null)
                        Destroy(itemT.gameObject);
                }

                _activeItem = value;
                Instantiate(_activeItem.ItemVisual, _rightHand.position, _rightHand.rotation, _rightHand);
                ActiveMode = GetMode(_activeItem.ActivatesMode);
                ActiveMode.SetItem(_activeItem);
            }
        }

        [SerializeField]
        private List<ItemPrototype> _equipment;

        void Start()
        {            
            _modes = new List<CharacterMode>{new BuilderMode(_character), new MiningMode(_character)};
            ActiveItem = _equipment[0]; 
            
            GameInputSingleton.Input.PC.SwitchItem.performed += OnSwitchItemAction;
        }
                
        void Update()
        {
            _activeMode?.Update();
        }

        private void OnSwitchItemAction(InputAction.CallbackContext obj)
        {
            int i = _equipment.IndexOf(_activeItem);
            i++;
            i %= _equipment.Count;
            ActiveItem = _equipment[i];
        }

        private CharacterMode GetMode(CharacterModeId modeId)
        {
            return _modes.Find( x=> x.ModeId() == modeId);
        }
    }
}