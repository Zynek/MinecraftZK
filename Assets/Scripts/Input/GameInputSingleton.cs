﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInputSingleton
{
    private static GameInput _input;
    public static GameInput Input
    {
        get
        {
            if (_input is null)
            {
                _input = new GameInput();
                _input.Enable();
            }

            return _input;
        }
    }
}
