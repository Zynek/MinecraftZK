// GENERATED AUTOMATICALLY FROM 'Assets/Resources/Input/GameInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @GameInput : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @GameInput()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""GameInput"",
    ""maps"": [
        {
            ""name"": ""PC"",
            ""id"": ""3daa4890-7b4c-4c58-9439-cce06c101759"",
            ""actions"": [
                {
                    ""name"": ""SwitchItem"",
                    ""type"": ""Button"",
                    ""id"": ""c796bcff-3eee-4547-abfe-55a32a7af677"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Walk"",
                    ""type"": ""Value"",
                    ""id"": ""4b33db05-6d1a-4bbc-baf9-055684844e87"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""be4bd46f-511e-4d61-9624-d17439c77dd9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""UseHeldItemPrimary"",
                    ""type"": ""Button"",
                    ""id"": ""bc56b9d8-0783-4896-a724-b1ed9db8591a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""LookAround"",
                    ""type"": ""Value"",
                    ""id"": ""56db25fa-a3d1-43c3-bf88-5a7eb54f0682"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ToggleMenu"",
                    ""type"": ""Button"",
                    ""id"": ""3ac49ecc-d568-4643-8d2e-3aacc8b94adb"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""NextCube"",
                    ""type"": ""Button"",
                    ""id"": ""a1b824e2-579e-4898-a7d7-2c5cbe6e5a7d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""355fc072-0966-4a38-80f5-68dac37a78f0"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""SwitchItem"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""71ae1796-db4f-48ac-a41b-f6ad7122c468"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SwitchItem"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""eec8f0a1-ce2d-4cfb-9170-eb12358ab9f9"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""KeyboardWASD"",
                    ""id"": ""8f76edf5-d14c-40d2-bf12-930646ceae60"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""41fa7583-cdbe-4f9a-96b8-2f57b63e45c1"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""381d125c-54b1-40d8-92db-15eb1a1021da"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""43ab2914-8549-4468-993f-da901f71ab54"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""f60da38a-3eb3-4eca-8083-13e46a018c40"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Walk"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""631614eb-3f01-42f1-8eb3-d5402dfb1c3c"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e6fbc85d-d6b2-4868-a76c-f33e5affd0e9"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c147b0fb-f0cd-42a7-b38e-07fa58e9a3cf"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UseHeldItemPrimary"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f556375f-c558-4e37-b936-9b5c34a069f0"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UseHeldItemPrimary"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d51c3c33-3d0e-49cb-b5e9-43de97e631da"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LookAround"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""86060f05-71bc-414f-8cd3-87d9de101a68"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LookAround"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f2f754d0-0093-4631-86ff-a9a4c52beb31"",
                    ""path"": ""<Keyboard>/tab"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ToggleMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0dc5fd28-26bf-4803-878b-1ff456a1d598"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ToggleMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8fa722e0-eb73-4560-bdd6-b6e726c89046"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""NextCube"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""272138e5-da29-420f-862f-8bfb21ec9453"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""NextCube"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""PC"",
            ""bindingGroup"": ""PC"",
            ""devices"": []
        }
    ]
}");
        // PC
        m_PC = asset.FindActionMap("PC", throwIfNotFound: true);
        m_PC_SwitchItem = m_PC.FindAction("SwitchItem", throwIfNotFound: true);
        m_PC_Walk = m_PC.FindAction("Walk", throwIfNotFound: true);
        m_PC_Jump = m_PC.FindAction("Jump", throwIfNotFound: true);
        m_PC_UseHeldItemPrimary = m_PC.FindAction("UseHeldItemPrimary", throwIfNotFound: true);
        m_PC_LookAround = m_PC.FindAction("LookAround", throwIfNotFound: true);
        m_PC_ToggleMenu = m_PC.FindAction("ToggleMenu", throwIfNotFound: true);
        m_PC_NextCube = m_PC.FindAction("NextCube", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // PC
    private readonly InputActionMap m_PC;
    private IPCActions m_PCActionsCallbackInterface;
    private readonly InputAction m_PC_SwitchItem;
    private readonly InputAction m_PC_Walk;
    private readonly InputAction m_PC_Jump;
    private readonly InputAction m_PC_UseHeldItemPrimary;
    private readonly InputAction m_PC_LookAround;
    private readonly InputAction m_PC_ToggleMenu;
    private readonly InputAction m_PC_NextCube;
    public struct PCActions
    {
        private @GameInput m_Wrapper;
        public PCActions(@GameInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @SwitchItem => m_Wrapper.m_PC_SwitchItem;
        public InputAction @Walk => m_Wrapper.m_PC_Walk;
        public InputAction @Jump => m_Wrapper.m_PC_Jump;
        public InputAction @UseHeldItemPrimary => m_Wrapper.m_PC_UseHeldItemPrimary;
        public InputAction @LookAround => m_Wrapper.m_PC_LookAround;
        public InputAction @ToggleMenu => m_Wrapper.m_PC_ToggleMenu;
        public InputAction @NextCube => m_Wrapper.m_PC_NextCube;
        public InputActionMap Get() { return m_Wrapper.m_PC; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PCActions set) { return set.Get(); }
        public void SetCallbacks(IPCActions instance)
        {
            if (m_Wrapper.m_PCActionsCallbackInterface != null)
            {
                @SwitchItem.started -= m_Wrapper.m_PCActionsCallbackInterface.OnSwitchItem;
                @SwitchItem.performed -= m_Wrapper.m_PCActionsCallbackInterface.OnSwitchItem;
                @SwitchItem.canceled -= m_Wrapper.m_PCActionsCallbackInterface.OnSwitchItem;
                @Walk.started -= m_Wrapper.m_PCActionsCallbackInterface.OnWalk;
                @Walk.performed -= m_Wrapper.m_PCActionsCallbackInterface.OnWalk;
                @Walk.canceled -= m_Wrapper.m_PCActionsCallbackInterface.OnWalk;
                @Jump.started -= m_Wrapper.m_PCActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_PCActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_PCActionsCallbackInterface.OnJump;
                @UseHeldItemPrimary.started -= m_Wrapper.m_PCActionsCallbackInterface.OnUseHeldItemPrimary;
                @UseHeldItemPrimary.performed -= m_Wrapper.m_PCActionsCallbackInterface.OnUseHeldItemPrimary;
                @UseHeldItemPrimary.canceled -= m_Wrapper.m_PCActionsCallbackInterface.OnUseHeldItemPrimary;
                @LookAround.started -= m_Wrapper.m_PCActionsCallbackInterface.OnLookAround;
                @LookAround.performed -= m_Wrapper.m_PCActionsCallbackInterface.OnLookAround;
                @LookAround.canceled -= m_Wrapper.m_PCActionsCallbackInterface.OnLookAround;
                @ToggleMenu.started -= m_Wrapper.m_PCActionsCallbackInterface.OnToggleMenu;
                @ToggleMenu.performed -= m_Wrapper.m_PCActionsCallbackInterface.OnToggleMenu;
                @ToggleMenu.canceled -= m_Wrapper.m_PCActionsCallbackInterface.OnToggleMenu;
                @NextCube.started -= m_Wrapper.m_PCActionsCallbackInterface.OnNextCube;
                @NextCube.performed -= m_Wrapper.m_PCActionsCallbackInterface.OnNextCube;
                @NextCube.canceled -= m_Wrapper.m_PCActionsCallbackInterface.OnNextCube;
            }
            m_Wrapper.m_PCActionsCallbackInterface = instance;
            if (instance != null)
            {
                @SwitchItem.started += instance.OnSwitchItem;
                @SwitchItem.performed += instance.OnSwitchItem;
                @SwitchItem.canceled += instance.OnSwitchItem;
                @Walk.started += instance.OnWalk;
                @Walk.performed += instance.OnWalk;
                @Walk.canceled += instance.OnWalk;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @UseHeldItemPrimary.started += instance.OnUseHeldItemPrimary;
                @UseHeldItemPrimary.performed += instance.OnUseHeldItemPrimary;
                @UseHeldItemPrimary.canceled += instance.OnUseHeldItemPrimary;
                @LookAround.started += instance.OnLookAround;
                @LookAround.performed += instance.OnLookAround;
                @LookAround.canceled += instance.OnLookAround;
                @ToggleMenu.started += instance.OnToggleMenu;
                @ToggleMenu.performed += instance.OnToggleMenu;
                @ToggleMenu.canceled += instance.OnToggleMenu;
                @NextCube.started += instance.OnNextCube;
                @NextCube.performed += instance.OnNextCube;
                @NextCube.canceled += instance.OnNextCube;
            }
        }
    }
    public PCActions @PC => new PCActions(this);
    private int m_PCSchemeIndex = -1;
    public InputControlScheme PCScheme
    {
        get
        {
            if (m_PCSchemeIndex == -1) m_PCSchemeIndex = asset.FindControlSchemeIndex("PC");
            return asset.controlSchemes[m_PCSchemeIndex];
        }
    }
    public interface IPCActions
    {
        void OnSwitchItem(InputAction.CallbackContext context);
        void OnWalk(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnUseHeldItemPrimary(InputAction.CallbackContext context);
        void OnLookAround(InputAction.CallbackContext context);
        void OnToggleMenu(InputAction.CallbackContext context);
        void OnNextCube(InputAction.CallbackContext context);
    }
}
